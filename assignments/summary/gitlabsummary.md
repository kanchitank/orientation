# GitLab

- GitLab is a **Git-based repository** manager and a powerful complete application for software development.
- GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain.
- With an "user-and-newbie-friendly" interface, GitLab allows us to work effectively, both from the command line and from the UI itself. 
- It's not only useful for developers, but can also be integrated across our entire team to bring everyone into a single and unique platform.

<img src = "https://gitlab.com/kanchitank/orientation/-/raw/master/images/3.jpg" width="700">